import pymongo
from flask import Flask, jsonify, request
import requests
from requests.exceptions import HTTPError
import json
import sys
import threading

flask = Flask(__name__)

#---------CONFIGURATION-------------
telegramBotURL = "http://127.0.0.1:7000"
httpEndpointURL = "http://127.0.0.1:3000"
#-----------------------------------

    #----------------------Sensor Data Logging------------------------
# Receive data to be inserted to the DB
@flask.route('/insertData', methods=['POST'])
def insertData():

    # read the request's data and insert them to the DB
    data = request.get_json() #get payload, as dict

    # If the data is flaged as urgent, notify the user and then remove the 'urgent' key
    if "urgent" in data:
        notifyuser(data["location"] + " " +data["sensor"] + " " +str(data["value"]))
        del data['urgent']

    # Insert Data to the DB
    try:
        insertDocument(data=data) #inserting dict to DB
    except Exception as e:
        print("Exception: ", e)

    # Get the automation rules applying to this sensor input
    # by first removing the unnecessary fields
    del data['datetime']
    del data['_id']
    rules = getRules(data)

    # Post to the HTTP Endpoint all the commands for the end-devices
    for x in rules:
        # Search for the actuator characteristics on the DB
        query = {'location':x['location'], 'actuator':x['actuator']}
        actuator = findDocument(query=query)
        # Add the ip in "x" dictionary
        x.update({"ip":actuator["ip"]})
        postData(URL=httpEndpointURL+'/postCommand', data=x)

    return flask.response_class(status = 200) #HTTP Response

    #----------------------Sensor Data Retrieval------------------------
# Get the latest sensor data with the location and sensor type
@flask.route('/retrieveData', methods=['GET'])
def retrieveData():

    # Empty dict, to add more args later
    query ={}

    # Get all the supported arguments from the request
    # Get location naming and add to the query if exists
    location = request.args.get('location', None)
    if (location != None):
        query.update({'location': location})

    # Get sensor naming and add to the query if exists
    sensor = request.args.get('sensor', None)
    if (sensor != None):
        query.update({'sensor': sensor})

    # Get results limit
    limit = request.args.get('limit', None)
    if (limit != None): # convert to int from unicode
        limit = int(limit)
    else: # if it wasn't defined just set to 1
        limit = 1

    # Get the result from the DB, it's a list of dicts
    result = findDocuments(query=query, limit=limit) # result from DB, as a list
    # Remove the _id field, that Mongo automatically adds to every Document
    for x in result:
        del x["_id"]

    # Create a HTTP Response
    response = flask.response_class(
        response = json.dumps(result), # The payload is converted to json
        status = 200, # Status code set to 200
        mimetype = 'application/json' # Content Type set to json
    )

    return response

    #----------------------New Actuator Deployment------------------------
# Receive new actuator characteristics and update them on the DB
# There can be only one device with the given location-actuator key
@flask.route('/actuatorDeployed', methods=['POST'])
def actuatorDeployed():

    # Get payload
    data = request.get_json() # type 'dict'

    # Replace the old document with a new one
    try:
        deleteDocument(query={'location':data['location'], 'actuator':data['actuator']})
        insertDocument(data=data, collection='actuators')
    except Exception as e:
        print("Exception: ", e)
        return flask.response_class(status = 500) #HTTP Response

    x = updateDocument(query={'location':data['location'], 'actuator':data['actuator']}, newvalues=data)
    return flask.response_class(status = 200) #HTTP Response

    #----------------------Actuator Activation------------------------
# Receive a command for activating actuators
@flask.route('/postCommand', methods=['POST'])
def postCommands():

    # Get payload
    data = request.get_json() # type 'dict'

    # Validate data, check if all required keys are present
    # If a key is absent then a status code 400 is returned to client
    requirements = {'location', 'actuator', 'value'}
    for x in requirements:
        if not(x in data):
            return flask.response_class(status = 400, response="Wrong input") #HTTP Response

    # Search for the actuator characteristics on the DB
    query = {'location':data['location'], 'actuator':data['actuator']}
    actuator = findDocument(query=query)

    # remove unnecessary items and add the neccessary
    # final items are 'location' 'actuator' 'value' 'ip'
    del actuator['_id']
    del actuator['range']
    actuator.update({'value': data['value']})

    # Post to the HTTP Endpoint
    postData(httpEndpointURL+'/postCommand', actuator)

    return flask.response_class(status = 200) #HTTP Response

    #----------------------Automation------------------------
# Add a new automation Rule
@flask.route('/addRule', methods=['POST'])
def addRule():

    # Get payload
    data = request.get_json() # type 'dict'

    # Validate data, check if all required keys are present
    # If a key is absent then a status code 400 is returned to client
    requirements = {'if', 'then'}
    for x in requirements:
        if not(x in data):
            return flask.response_class(status = 400, response="Wrong input") #HTTP Response

    # Insert new rule document to the DB,
    # remove possible duplicate of the rule
    deleteDocument(query=data, collection='rules')
    insertDocument(data=data, collection='rules')

    return flask.response_class(status = 200) #HTTP Response

# Returns the 'then' fields corresponding to the query
# Query must be like: 'location' 'sensor' 'value'
def getRules(query):

    # List of all 'then' dicts, which consist of 'location' 'actuator' 'value'
    then = []

    print("query",{"if":query})
    # All the rules corresponding to the query input
    rules = findDocuments(query={"if":query}, collection='rules', limit = 10)

    # Append the 'then' values to the list, '_id' and 'if' are not used
    for rule in rules:
        then.append(rule['then'])

    return then


    #----------------------General Functions------------------------

    #---------post Data----------
# Post a dict as json to a given URL
def postData(URL, data):
    headers = {'Content-type': 'application/json'} # neccessary fot compatibility
    try:
        # the POST is executed the server response is stored
        response = requests.post(url=URL, data=json.dumps(data), headers=headers)
         # If the response was successful, no Exception will be raised
        response.raise_for_status()
        code = response.status_code
        print('HTTP Code:', code)
    except HTTPError as http_err:
        print('Error posting')

    #---------Flask Testing Fuctions----------
# Return the client ip, FOR TESTING
@flask.route("/ip", methods=["GET"])
def ip():
    return jsonify({'ip': request.remote_addr}), 200

# Return the request arguments, FOR TESTING
@flask.route("/args", methods=["GET"])
def args():
    # request.args is an ImmutableMultiDict object
    location = request.args.get('location',None)
    sensor = request.args.get('sensor',None)
    print (location, sensor)

    return "K"

    #---------DB Fuctions----------

# insert document to collection
def insertDocument(data, database = "homeAutomation", URL = "mongodb://localhost:27017/", collection = "iotData"):
    # connection to the mongo server
    myclient = pymongo.MongoClient(URL)

    # DB interface
    db = myclient[database]

    # Collection for IOT incomming data
    iotdata = db[collection]

    # Insert data
    insert = iotdata.insert_one(data) #db.post instance

# Return a list from DB
def findDocuments(database = "homeAutomation", URL = "mongodb://localhost:27017/", collection = "iotData", query = {}, limit = 1, sort = "datetime", order = "descending"):

    #connection to the mongo server
    myclient = pymongo.MongoClient(URL)
    #DB interface
    db = myclient[database]
    # Collection Selection
    col = db[collection]

    # the ascending and descending order are represented with 1/-1 in sort() function
    if(order == "ascending"):
        order = 1
    else:
        order = -1

    # col.find() returns a pymongo.cursor.Cursor object
    # for more info on how this line of code works read pymongo.md file in Documentation
    # return a list of the documents found in the DB
    return list(col.find(query).sort(sort,order).limit(limit))

# Return one document, as 'dict;'
def findDocument(database = "homeAutomation", URL = "mongodb://localhost:27017/", collection = "actuators", query = {}):

        # Connection to the mongo server
        myclient = pymongo.MongoClient(URL)
        # DB interface
        db = myclient[database]
        # Collection Selection
        col = db[collection]

        # Returns the Document found by the query
        return col.find_one(query)

# Update the document found by 'query' with 'newvalues'
def updateDocument(database = "homeAutomation", URL = "mongodb://localhost:27017/", collection = "actuators", query = {}, newvalues={}):

    # Connection to the mongo server
    myclient = pymongo.MongoClient(URL)
    # DB interface
    db = myclient[database]
    # Collection Selection
    col = db[collection]

    # The 'set' dict defines the new items added or replaced in the document
    set = {"$set":newvalues} # PyMongo data structure
    col.update_one(query, set)

# Delete the document found by 'query'
def deleteDocument(database = "homeAutomation", URL = "mongodb://localhost:27017/", collection = "actuators", query = {}):

    #connection to the mongo server
    myclient = pymongo.MongoClient(URL)
    #DB interface
    db = myclient[database]
    # Collection Selection
    col = db[collection]

    # Returns the Document found by the query
    col.delete_one(query)

    #---------Telegram Fuctions----------
# Send a text message to the telegram chat, via telegram Bot
def notifyuser(text):

    headers = {'Content-type': 'text/plain'}

    try:
        # the POST is executed, the server response is stored
        response = requests.post(url=telegramBotURL+'/notify', data=text, headers=headers)

        # Raises stored HTTPError, if one occurred.
        response.raise_for_status()

        # HTTP code
        code = response.status_code
        print('HTTP Code:', code)

    except HTTPError as http_err:
        print('Error posting to event manager')

if __name__ == '__main__':
    #flask.run(debug = True, host='0.0.0.0', port = 5000)
    threading.Thread(target=flask.run(debug = True, host='127.0.0.1', port = 5000)).start()
